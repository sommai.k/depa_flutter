import 'package:hooks_riverpod/hooks_riverpod.dart';

final firstNameProvider = StateProvider<String>((ref) => "");
final lastNameProvider = StateProvider<String>((ref) => "");
final tokenProvider = StateProvider<String>((ref) => "");
