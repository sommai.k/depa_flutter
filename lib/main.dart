import 'package:depa_flutter/screen/login.dart';
import 'package:depa_flutter/screen/my_full_page.dart';
import 'package:depa_flutter/screen/user_screen.dart';
import 'package:flutter/material.dart';
import './screen/my_home_page.dart';
import './screen//register.dart';
import './screen/app_main_screen.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import './screen//post_screen.dart';

void main() {
  runApp(
    const ProviderScope(
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      initialRoute: "/login",
      routes: {
        "/": (context) => const MyFullPage(),
        "/myhomepage": (context) => MyHomePage(title: ""),
        "/login": (context) => Login(),
        "/register": (context) => const Register(),
        "/main": (context) => const AppMainScreen(),
        "/post": (context) => PostScreen(),
        "/user": (context) => UserScreen(),
      },
    );
  }
}
