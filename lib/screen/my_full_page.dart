import 'package:flutter/material.dart';

class MyFullPage extends StatefulWidget {
  const MyFullPage({super.key});

  @override
  State<MyFullPage> createState() => _MyFullPageState();
}

class _MyFullPageState extends State<MyFullPage> {
  int cnt = 10;

  void whenAddPress() {
    setState(() {
      cnt = cnt + 1;
      print(cnt);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Full Page"),
      ),
      body: Column(
        children: [
          Text("$cnt"),
          ElevatedButton(
            onPressed: whenAddPress,
            child: const Text("Add"),
          )
        ],
      ),
    );
  }
}
