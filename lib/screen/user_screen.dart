import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:dio/dio.dart';
import '../app/login_provider.dart';

class UserScreen extends HookConsumerWidget {
  const UserScreen({super.key});

  Future<List> loadUser(WidgetRef ref) async {
    try {
      final token = ref.read(tokenProvider);
      final resp = await Dio().get(
        "https://bde299da1f63db3df1d7b4eb536ecf0b.serveo.net/user",
        options: Options(
          headers: {"Authorization": "Bearer $token"},
        ),
      );
      return resp.data ?? [];
    } catch (e) {
      throw e;
    }
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      appBar: AppBar(
        title: Text("ผู้ใช้งาน"),
      ),
      body: FutureBuilder(
        future: loadUser(ref),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
              itemCount: snapshot.data!.length,
              itemBuilder: (context, index) {
                var d = snapshot.data![index];
                return ListTile(
                  leading: Text("${d["age"]}"),
                  title: Text("${d["firstName"]} ${d["lastName"]}"),
                  subtitle: Text("${d["userCode"]}"),
                );
              },
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }
}
