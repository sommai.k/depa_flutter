import 'package:flutter/material.dart';
import './home_screen.dart';
import './menu_screen.dart';
import './message_screen.dart';
import './setting_screen.dart';

class AppMainScreen extends StatelessWidget {
  const AppMainScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return const DefaultTabController(
      length: 4,
      child: Scaffold(
        body: TabBarView(
          children: [
            HomeScreen(),
            MenuScreen(),
            MessageScreen(),
            SettingScreen(),
          ],
        ),
        bottomNavigationBar: TabBar(
          tabs: [
            Tab(
              icon: Icon(Icons.home),
              text: "หน้าหลัก",
            ),
            Tab(
              icon: Icon(Icons.menu),
              text: "เมนู",
            ),
            Tab(
              icon: Icon(Icons.message),
              text: "ข้อความ",
            ),
            Tab(
              icon: Icon(Icons.settings),
              text: "ตั้งค่า",
            ),
          ],
        ),
      ),
    );
  }
}
