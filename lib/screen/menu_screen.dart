import 'package:flutter/material.dart';
import '../ui/menu_item.dart';

class MenuScreen extends StatelessWidget {
  const MenuScreen({super.key});

  void goTo(BuildContext context, String path) {
    Navigator.of(context).pushNamed(path);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 25),
      child: GridView.count(
        crossAxisCount: 3,
        children: [
          MenuItem(
            label: "User",
            icon: Icons.people,
            color: Colors.lightBlue.shade600,
            onPress: () => goTo(context, "/user"),
          ),
          MenuItem(
            label: "Todo",
            icon: Icons.list,
            color: Colors.yellow.shade600,
          ),
          MenuItem(
            label: "Photo",
            icon: Icons.camera,
            color: Colors.purple.shade600,
          ),
          MenuItem(
            label: "Album",
            icon: Icons.image,
            color: Colors.red.shade600,
          ),
          MenuItem(
            label: "Comment",
            icon: Icons.comment,
            color: Colors.green.shade600,
          ),
          MenuItem(
            label: "Post",
            icon: Icons.post_add,
            color: Colors.brown.shade600,
            onPress: () => goTo(context, "/post"),
          ),
        ],
      ),
    );
  }
}
