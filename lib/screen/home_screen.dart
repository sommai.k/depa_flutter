import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:depa_flutter/app/login_provider.dart';

class HomeScreen extends HookConsumerWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final firstName = ref.read(firstNameProvider);
    final lastName = ref.read(lastNameProvider);

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Text("สวัสดี คุณ $firstName $lastName"),
          ],
        ),
      ),
    );
  }
}
