import 'package:flutter/material.dart';

class MyHomePage extends StatelessWidget {
  final String title;
  int cnt = 10;

  MyHomePage({super.key, required this.title});

  void whenAddPress() {
    cnt = cnt + 1;
    print(cnt);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Text("$title $cnt"),
          ElevatedButton(
            onPressed: whenAddPress,
            child: const Text("Add"),
          )
        ],
      ),
    );
  }
}
